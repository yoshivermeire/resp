package filesystem;
import be.kuleuven.cs.som.annotate.*;
import java.util.*;

/**
 * An abstract class. A ContenHolder with a name, a directory and creationTime and modificationTIme
 * @invar	Each ContentHolder must have a properly spelled name.
 * 			| isValidName(getName())
 * @invar   Each ContentHolder must have a valid creation time.
 *          | isValidCreationTime(getCreationTime())
 * @invar   Each ContentHolder must have a valid modification time.
 *          | canHaveAsModificationTime(getModificationTime())
 * @author Yoshi Vermeire and  Pieter-Jan Vrielynck
 *
 */
public abstract  class ContentHolder {
    /**
     * Return the name of this file.
     * @note		See Coding Rule 19 for the Basic annotation.
     */
    @Raw @Basic 
    public String getName() {
        return name;
    }
    /**
     * Check whether the given name is a legal name for a file.
     * 
     * @param  	name
     *			The name to be checked
     * @return	True if the given string is effective, not
     * 			empty and consisting only of letters, digits, dots,
     * 			hyphens and underscores; false otherwise.
     * 			| result ==
     * 			|	(name != null) && name.matches("[a-zA-Z_0-9.-]+")
     */
    public static boolean isValidName(String name) {
        return (name != null && name.matches("[a-zA-Z_0-9.-]+"));
    }
    /**
     * Set the name of this file to the given name.
     *
     * @param   name
     * 			The new name for this file.
     * @post    If the given name is valid, the name of
     *          this file is set to the given name,
     *          otherwise the name of the file is set to a valid name (the default).
     *          | if (isValidName(name))
     *          |      then new.getName().equals(name)
     *          |      else new.getName().equals(getDefaultName())
     */
    @Raw @Model 
    private void setName(String name) {
        if (isValidName(name)) {
        		this.name = name;
        } else {
        		this.name = getDefaultName();
        }
    }
    /**
     * Return the name for a new file which is to be used when the
     * given name is not valid.
     *
     * @return   A valid file name.
     *         | isValidName(result)
     */
    @Model
    private static String getDefaultName() {
        return "new_file";
    }
    /**
     * Change the name of this file to the given name.
     *
     * @param	name
     * 			The new name for this file.
     * @effect  The name of this file is set to the given name, 
     * 			if this is a valid name and the file is writable, 
     * 			otherwise there is no change.
     * 			| if (isValidName(name) && isWritable())
     *          | then setName(name)
     * @effect  If the name is valid and the file is writable, the modification time 
     * 			of this file is updated.
     *          | if (isValidName(name) && isWritable())
     *          | then setModificationTime()
     * @throws  FileNotWritableException(this)
     *          This file is not writable
     *          | ! isWritable() 
     */
    public void changeName(String name) throws ContentNotWritableException {
        if (isWritable()) {
            if (isValidName(name)){
            	setName(name);
                setModificationTime();
            }
        } else {
            throw new ContentNotWritableException(this);
        }
    }
	private String name = null; 
    /**
     * Return the time at which this file was created.
     */
    @Raw @Basic @Immutable
    public Date getCreationTime() {
        return creationTime;
    }
    /**
     * Check whether the given date is a valid creation time.
     *
     * @param  	date
     *         	The date to check.
     * @return 	True if and only if the given date is effective and not
     * 			in the future.
     *         	| result == 
     *         	| 	(date != null) &&
     *         	| 	(date.getTime() <= System.currentTimeMillis())
     */
    public static boolean isValidCreationTime(Date date) {
    	return 	(date!=null) &&
    			(date.getTime()<=System.currentTimeMillis());
    }
	private final Date creationTime = new Date();
    /**
     * Return the time at which this file was last modified, that is
     * at which the name or size was last changed. If this file has
     * not yet been modified after construction, null is returned.
     */
    @Raw @Basic
    public Date getModificationTime() {
        return modificationTime;
    }
    /**
     * Check whether this file can have the given date as modification time.
     *
     * @param	date
     * 			The date to check.
     * @return 	True if and only if the given date is either not effective
     * 			or if the given date lies between the creation time and the
     * 			current time.
     *         | result == (date == null) ||
     *         | ( (date.getTime() >= getCreationTime().getTime()) &&
     *         |   (date.getTime() <= System.currentTimeMillis())     )
     */
    public boolean canHaveAsModificationTime(Date date) {
        return (date == null) ||
               ( (date.getTime() >= getCreationTime().getTime()) &&
                 (date.getTime() <= System.currentTimeMillis()) );
    }
    /**
     * Set the modification time of this file to the current time.
     *
     * @post   The new modification time is effective.
     *         | new.getModificationTime() != null
     * @post   The new modification time lies between the system
     *         time at the beginning of this method execution and
     *         the system time at the end of method execution.
     *         | (new.getModificationTime().getTime() >=
     *         |                    System.currentTimeMillis()) &&
     *         | (new.getModificationTime().getTime() <=
     *         |                    (new System).currentTimeMillis())
     */
    @Model 
    private void setModificationTime() {
        modificationTime = new Date();
    }
    /**
     * Return whether this file and the given other file have an
     * overlapping use period.
     *
     * @param 	other
     *        	The other file to compare with.
     * @return 	False if the other file is not effective
     * 			False if the prime object does not have a modification time
     * 			False if the other file is effective, but does not have a modification time
     * 			otherwise, true if and only if the open time intervals of this file and
     * 			the other file overlap
     *        	| if (other == null) then result == false else
     *        	| if ((getModificationTime() == null)||
     *        	|       other.getModificationTime() == null)
     *        	|    then result == false
     *        	|    else 
     *        	| result ==
     *        	| ! (getCreationTime().before(other.getCreationTime()) && 
     *        	|	 getModificationTime().before(other.getCreationTime()) ) &&
     *        	| ! (other.getCreationTime().before(getCreationTime()) && 
     *        	|	 other.getModificationTime().before(getCreationTime()) )
     */
    public boolean hasOverlappingUsePeriod(ContentHolder other) {
        if (other == null) return false;
        if(getModificationTime() == null || other.getModificationTime() == null) return false;
        return ! (getCreationTime().before(other.getCreationTime()) && 
        	      getModificationTime().before(other.getCreationTime()) ) &&
        	   ! (other.getCreationTime().before(getCreationTime()) && 
        	      other.getModificationTime().before(getCreationTime()) );
    }
	private Date modificationTime = null;
    /**
     * Check whether this file is writable.
     */
    @Raw @Basic
    public boolean isWritable() {
        return isWritable;
    }
    /**
     * Set the writability of this file to the given writability.
     *
     * @param isWritable
     *        The new writability
     * @post  The given writability is registered as the new writability
     *        for this file.
     *        | new.isWritable() == isWritable
     */
    @Raw 
    public void setWritable(boolean isWritable) {
        this.isWritable = isWritable;
    }

	private boolean isWritable = true;
	/**
	 * This returns the directory where the contentholder is currently saved. 
	 * @return the directory 
	 * 
	 */
	public Directory getDirectory() throws RootContentException {
		if (this.isRoot()) { throw new RootContentException() ;}
		
	}
	/**
	 * This method checks wether a contentholder is a rootdirectory or a rootfile 
	 * @return true if the contentholder is a rootfile or rootdirectory 
	 *         false  all other cases. 
	 *         | if getDirectory == null then isRoot() 
	 *         | else !isRoot() 
	 */
	public boolean isRoot() {
		if (this.getDirectory() == null ) {return true} ;  
		else {return false} ; 
		
	}
	private Directory directory = null ; 
	

}
