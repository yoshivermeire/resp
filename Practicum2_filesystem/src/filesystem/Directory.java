package filesystem;

import java.util.Date;
import java.util.*;

import be.kuleuven.cs.som.annotate.*;

/**
 * Class representing a directory.
 * @author	Pieter-Jan Vrielynck and Yoshi Vermeire
 * @invar	Each directory must have a properly spelled name.
 * 			| isValidName(getName())
 * @invar   Each file must have a valid creation time.
 *          | isValidCreationTime(getCreationTime())
 * @invar   Each file must have a valid modification time.
 *          | canHaveAsModificationTime(getModificationTime())
 *          
 * @version 1.0
 *
 */
public class Directory extends ContentHolder {

	
//Constructors
	
	
	/**
	 * Constructor to initialize a Directory instance.
	 * @param	name
	 * 			The name for the Directory instance.
	 * @effect  The name of the directory is set to the given name.
     * 			If the given name is not valid, a default name is set.
     *          | setName(name)
	 *
	 * @post    The new creation time of this file is initialized to some time during
     *          constructor execution.
     *          | (new.getCreationTime().getTime() >= System.currentTimeMillis()) &&
     *          | (new.getCreationTime().getTime() <= (new System).currentTimeMillis())
     * @post   	The new file has no time of last modification.
     *          | new.getModificationTime() == null
     */
	public Directory(String name) {
		setName(name);
	}
	
	
	/**
	 * Constructor to initialize a Directory instance.
	 * @param	name
	 * 			The name for the Directory instance.
	 * @param  	writable
     *         	The writability of the new file.
	 * @effect  The name of the directory is set to the given name.
     * 			If the given name is not valid, a default name is set.
     *          | setName(name)
	 *@effect	The writability is set to the given flag
     * 			| setWritable(writable)
	 * @post    The new creation time of this file is initialized to some time during
     *          constructor execution.
     *          | (new.getCreationTime().getTime() >= System.currentTimeMillis()) &&
     *          | (new.getCreationTime().getTime() <= (new System).currentTimeMillis())
     * @post   	The new file has no time of last modification.
     *          | new.getModificationTime() == null
     */
	public Directory(String name, boolean writable) {
		setName(name);
		setWritable(writable);
	}
	
	/**
	 * Constructor to initialize a Directory instance.
	 * @param	name
	 * 			The name for the Directory instance.
	 * @param  	writable
     *         	The writability of the new file.
	 * @effect  The name of the directory is set to the given name.
     * 			If the given name is not valid, a default name is set.
     *          | setName(name)
	 *@effect	The writability is set to the given flag
     * 			| setWritable(writable)
	 * @post    The new creation time of this file is initialized to some time during
     *          constructor execution.
     *          | (new.getCreationTime().getTime() >= System.currentTimeMillis()) &&
     *          | (new.getCreationTime().getTime() <= (new System).currentTimeMillis())
     * @post   	The new file has no time of last modification.
     *          | new.getModificationTime() == null
     */
	public Directory(Directory dir, String name, boolean writable) throws DirectOrIndirectSubdirectoryException{
		if (!isDirectOrIndirectSubdirectoryOf) { //MOET NOG OPGELOST WORDEN!
			setName(name);
			setWritable(writable);
		} else {
			throw new DirectOrIndirectSubdirectory(this);
		}
		
	}
	
	
	
	
	//
	private String name = null;
	private final Date creationTime = new Date();
	private Date modificationTime = null;
	private ArrayList<ContentHolder> content;

}
